import React from 'react';
// import './style.css';

interface InputWithIcon {
  icon: string;
  placeholder: string;
  className?: string;
}

const InputWithIcon = (props: InputWithIcon) => {
  const { icon, placeholder, className } = props;

  return (
    <div className={`input-container ${className}`}>
      <i className={`fa ${icon}`}></i>
      <input type="text" className="form-control" placeholder={placeholder} />
    </div>
  );
}

export default InputWithIcon;


