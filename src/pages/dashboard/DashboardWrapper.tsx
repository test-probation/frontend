/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {useIntl} from 'react-intl'
import {PageTitle} from '../../_metronic/layout/core'
import InputWithIcon from '../../components/form-icon/FormIcon'
import { KTCard, KTCardBody } from '../../_metronic/helpers'
import Dashboard from './components/dashboard'
import { QueryRequestProvider } from './core/QueryRequestProvider'
import { QueryResponseProvider } from './core/QueryResponseProvider'
import { ResourceProvider } from '../../context/ResourceContext'

const DashboardPage: FC = () => (
  <>
    <KTCard>
      <KTCardBody>
        <Dashboard />
      </KTCardBody>
    </KTCard>
  </>
)

const DashboardWrapper: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({id: 'MENU.DASHBOARD'})}</PageTitle>

      <ResourceProvider>
        <QueryRequestProvider>
          <QueryResponseProvider>
            <DashboardPage />
          </QueryResponseProvider>
        </QueryRequestProvider>
      </ResourceProvider>
    </>
  )
}

export {DashboardWrapper}
