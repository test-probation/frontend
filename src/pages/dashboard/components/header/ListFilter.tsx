import { useEffect, useState } from 'react'
import { MenuComponent } from '../../../../_metronic/assets/ts/components'
import { initialQueryState, KTSVG } from '../../../../_metronic/helpers'
import { useQueryRequest } from '../../core/QueryRequestProvider'
import { useQueryResponse } from '../../core/QueryResponseProvider'
import { useResourceContext } from '../../../../context/ResourceContext'
import { useFormatter } from '../../../../hooks/useFormatter'
import { get } from '../../core/_requests'
import { Form } from 'react-bootstrap'

const initFilter = {
  id_region: undefined,
  id_province: undefined,
}
const ListFilter = () => {
  const { updateState } = useQueryRequest()
  const { isLoading } = useQueryResponse()
  const { collection } = useResourceContext()
  const [filter, setFilter] = useState(initFilter)
  const [optionRegion, setOptionRegion]: Array<any> = useState([]);
  const [optionProvince, setOptionProvince]: Array<any> = useState([]);


  const handleChange = (e: any) => {
    const key = e.target ? e.target.name : e.name
    const value = e.target ? e.target.value : e.value

    if(key === 'id_region') {
      setFilter((old) => ({...old, [key]: value, id_province: undefined}))
    } else {
      setFilter((old) => ({...old, [key]: value}))
    }
  }

  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])

  const resetData = () => {
    setFilter(initFilter)
    updateState({ filter: undefined, ...initialQueryState })
  }

  const filterData = () => {
    console.log("filter", filter);
    updateState({
      filter: {
        ...filter,
      },
      ...initialQueryState,
    })
  }

  const getOptionRegion = async () => {
    try {
      const { data } = await get("region");                
      const result : Record<string, any> = data.data
      setOptionRegion(result.result);
    } catch (e) {
      console.log(e)
    }
  };

  const getOptionProvince = async () => {
    try {
      let params = {}
      if (filter.id_region) {
        params = {
          id_region: filter.id_region,
          '!limit': 100,
        }
      }
      const { data } = await get("province", params);                
      const result : Record<string, any> = data.data
      setOptionProvince(result.result);
    } catch (e) {
        console.log(e)
    }
  };

  useEffect(() => {
    getOptionRegion();
    getOptionProvince();
  }, [])

  useEffect(() => {
    getOptionProvince();
  }, [filter.id_region])

  const { capitalizeFormatUndescoreString } = useFormatter()

  return (
    <>
      {/* begin::Filter Button */}
      <button
        disabled={isLoading}
        type='button'
        className='btn btn-bg-white btn-color-primary text-nowrap mx-5 border-primary border-hover'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        <KTSVG path='/media/icons/duotune/general/gen031.svg' className='svg-icon-2' />
        Filter {capitalizeFormatUndescoreString(collection)}
      </button>
      {/* end::Filter Button */}
      {/* begin::SubMenu */}
      <div className='menu menu-sub menu-sub-dropdown w-300px w-md-325px' data-kt-menu='true'>
        {/* begin::Header */}
        <div className='px-7 py-5'>
          <div className='fs-5 text-dark fw-bolder'>Filter Options</div>
        </div>
        {/* end::Header */}

        {/* begin::Separator */}
        <div className='separator border-gray-200'></div>
        {/* end::Separator */}

        {/* begin::Content */}
        <div className='px-7 py-5' data-kt-user-table-filter='form'>
          {/* begin::Form Group */}
          <Form.Group>
            <Form.Label>Wilayah</Form.Label>
            <Form.Select name='id_region' value={filter.id_region} onChange={handleChange}>
              <option key={-1} value={''}>Select Wilayah</option>
              {optionRegion.map((it: Record<string, any>, idx: number) => {
                return (
                  <option key={idx} value={it.id}>
                    {it.name}
                  </option>
                )
              })}
            </Form.Select>
          </Form.Group>

          <Form.Group>
            <Form.Label>Provinsi</Form.Label>
            <Form.Select name='id_province' value={filter.id_province} onChange={handleChange}>
              <option key={-1} value={''}>Select Province</option>
              {optionProvince.map((it: Record<string, any>, idx: number) => {
                return (
                  <option key={idx} value={it.id}>
                    {it.name}
                  </option>
                )
              })}
            </Form.Select>
          </Form.Group>
          {/* end::Form Group */}

          {/* begin::Actions */}
          <div className='d-flex justify-content-end'>
            <button
              type='button'
              disabled={isLoading}
              onClick={resetData}
              className='btn btn-light btn-active-light-primary fw-bold me-2 px-6'
              data-kt-menu-dismiss='true'
              data-kt-user-table-filter='reset'
            >
              Reset
            </button>
            <button
              disabled={isLoading}
              type='button'
              onClick={filterData}
              className='btn btn-primary fw-bold px-6'
              data-kt-menu-dismiss='true'
              data-kt-user-table-filter='filter'
            >
              Apply
            </button>
          </div>
          {/* end::Actions */}
        </div>
        {/* end::Content */}
      </div>
      {/* end::SubMenu */}
    </>
  )
}

export { ListFilter }
