import { useEffect, useMemo, useState } from "react"
import { get } from "../core/_requests"
import { Card, Col, Row } from "react-bootstrap"
import { ListFilter } from "./header/ListFilter"
import { useQueryResponseData, useQueryResponseLoading } from "../core/QueryResponseProvider"
import { ListLoading } from "./loading/ListLoading"

type DataCountInterface = {
    countRegion: number,
    countProvince: number,
    countCity: number
}

const initialValue = {
    countRegion: 0,
    countProvince: 0,
    countCity: 0
}

const Dashboard = () => {
    const response = useQueryResponseData();
    const isLoading = useQueryResponseLoading();
    const data : Record<string, any> = useMemo(() => response, [response])
    const dataCount : DataCountInterface = useMemo(() => data.result ?? initialValue, [data])

    
    console.log("Data", data.result, dataCount)
    // useEffect(() => {
    //     const fetchData = async () => {
    //         try {
    //             const { data } = await get("dashboard");                
    //             const result : Record<string, any> = data.data
    //             setDataCount(result.result);
    //         } catch (e) {
    //             console.log(e)
    //         }
    //     };
    //     fetchData();
    // }, [])

    return (
        <>
            <div className="d-flex align-items-center mb-5">
                {/* <ListSearchComponent/> */}
                <ListFilter />
            </div>
            {isLoading ? <ListLoading /> : (
                <Row>
                    <Col lg={3} md={4} sm={6}>
                        <Card bg="primary">
                            <Card.Body>
                                <Card.Title>Jumlah Wilayah</Card.Title>
                                <Card.Text>{dataCount.countRegion}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col lg={3} md={4} sm={6}>
                        <Card bg="warning">
                            <Card.Body>
                                <Card.Title>Jumlah Provinsi</Card.Title>
                                <Card.Text>{dataCount.countProvince}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col lg={3} md={4} sm={6}>
                        <Card bg="success">
                            <Card.Body>
                                <Card.Title>Jumlah Kota / Kabupaten</Card.Title>
                                <Card.Text>{dataCount.countCity}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            )}
        </>
    )
}

export default Dashboard